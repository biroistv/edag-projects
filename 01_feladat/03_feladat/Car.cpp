#include "stdafx.h"
#include "Car.h"

Car::Car() :
	Vehicle()
{
}

void Car::change_gear()
{
}

void Car::start_engine()
{
}

void Car::stop_engine()
{
}

void Car::open_airbag()
{
}

void Car::turn_lights_on()
{
}

void Car::turn_ac_on()
{
}
