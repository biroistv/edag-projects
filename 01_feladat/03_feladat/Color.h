#pragma once
class Color
{
public:
	Color(int, int, int);
	~Color() = default;

private:
	unsigned __int8 red_;
	unsigned __int8 green_;
	unsigned __int8 blue_;
};

