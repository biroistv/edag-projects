#pragma once

#include <string>
#include "Color.h"

class Vehicle
{
public:
	Vehicle() = default;
	~Vehicle() = default;

private:
	std::string owner_;
	Color color_;
	std::string license_plate_number_;
	std::string manufacturer_;
	std::string model_;

public:

	void set_owner(std::string owner) { this->owner_ = owner; }
	void set_color(Color color) { this->color_ = color; }
	void set_license_plate_number(std::string lpn) { this->license_plate_number_ = lpn; }
	void set_manufacturer(std::string manufacturer) { this->manufacturer_ = manufacturer; }
	void set_model(std::string model) { this->model_ = model; }

	std::string get_owner() const { return this->owner_; };
	Color get_color() const { return this->color_; };
	std::string get_license_plate_number() const { return this->license_plate_number_; };
	std::string get_manufacturer() const { return this->manufacturer_; };
	std::string get_model() const { return this->model_; };

	void move_forward(float);
	void move_backward(float);
	void _break(float);
	void turn(float);
	virtual void change_gear() = 0;
};

