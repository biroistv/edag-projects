#pragma once

#include "Vehicle.h"

enum class enumRoofType
{
	SEDAN,
	CONVERTIBLE,
	FASTBACK,
	HATCHBACK
};

enum class enumFuelType
{
	DIESEL,
	PETROL,
	GAS,
	ELECTRONIC
};

enum class enumTransmissionType
{
	AUTOMATIC,
	MANUAL
};

class Car : public Vehicle
{
public:
	Car();
	~Car() = default;

	unsigned __int8 get_doors_number_() const { return this->doors_number_; }
	unsigned __int8 get_seats_number_() const { return this->seats_number_; }
	enumRoofType get_roof_type_() const { return this->roof_type_; }
	enumFuelType get_fuel_type_() const { return this->fuel_type_; }
	enumTransmissionType get_transmission_type_() const { return this->transmission_type_; }

	void set_doors_number_(unsigned __int8 val) { this->doors_number_ = val; }
	void set_seats_number_(unsigned __int8 val) { this->seats_number_ = val; }
	void set_roof_type_(enumRoofType val) { this->roof_type_ = val; }
	void set_fuel_type_(enumFuelType val) { this->fuel_type_ = val; }
	void set_transmission_type_(enumTransmissionType val) { this->transmission_type_ = val; }

	virtual void change_gear() override;
	void start_engine();
	void stop_engine();
	void open_airbag();
	void turn_lights_on();
	void turn_ac_on();
	//...


private:

	unsigned __int8 doors_number_{};
	unsigned __int8 seats_number_{};
	enumRoofType roof_type_;
	enumFuelType fuel_type_;
	enumTransmissionType transmission_type_;
};

