#pragma once

#include <wx/string.h>

class FileNameHandler
{
public:
	FileNameHandler() = default;
	~FileNameHandler() = default;

	static wxString get_file_extension(wxString&);
	static uint16_t get_number_of_consonant(wxString&);

private:
	static bool is_vowel(const char&);
	static size_t get_extension_point(const wxString&);
};

