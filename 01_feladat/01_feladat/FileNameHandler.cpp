#include "FileNameHandler.h"

/*
	This method give back the extension of a file
*/
wxString FileNameHandler::get_file_extension(wxString& str)
{
	wxString output{ "" };

	size_t pos{ get_extension_point(str) };

	// Loop through the str from back to find the first point
	if (pos != 0)
	{
		for (pos += 1; pos < str.Length(); pos++)
			output.Append(str.at(pos));
	}

	return output;
}

/*
	This functions count and give back the number of consonant in the input string
*/
uint16_t FileNameHandler::get_number_of_consonant(wxString& str)
{
	uint16_t number_of_consonant{};

	// If the str is a file
	if (get_extension_point(str) != 0)
	{
		for (size_t pos{ get_extension_point(str) }; pos > 0; --pos)
		{
			if (!is_vowel(str.at(pos - 1)) && isalpha(str.at(pos - 1)))
				++number_of_consonant;
		}
	}
	// If the str is a folder
	else
	{
		for (size_t pos{ str.Length() }; pos > 0; --pos)
		{
			if (!is_vowel(str.at(pos - 1)) && isalpha(str.at(pos - 1)))
				++number_of_consonant;
		}
	}

	return number_of_consonant;
}

/*
	Determine if a character is a vowel or not. Only working on the english alphabet.
*/
bool FileNameHandler::is_vowel(const char& ch)
{
	const uint8_t SIZE{ 5 };

	const char vowels[SIZE]{ 'a','e','i','o','u' };

	for (int pos{}; pos < SIZE; ++pos)
	{
		if (tolower(ch) == vowels[pos])
			return true;
	}

	return false;
}

/*
	This give back the extension point position.

	If the file is a folder then it will return 0;
*/
size_t FileNameHandler::get_extension_point(const wxString& str)
{
	for (size_t i{ str.Length() }; i > 0; --i)
	{
		if (str.at(i - 1) == '.')
		{
			return i - 1;
		}
	}

	return 0;
}
