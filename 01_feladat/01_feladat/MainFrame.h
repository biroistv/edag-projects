#pragma once

#include <wx/wx.h>
#include <wx/ustring.h>

class MainFrame : public wxFrame
{
public:
	MainFrame(const wxString& window_title);
	~MainFrame() = default;

	void on_quit(wxCommandEvent& event);
	void on_about(wxCommandEvent& event);
	void on_list_button(wxCommandEvent& event);
	void on_open_button(wxCommandEvent& event);

private:
	wxMenuBar* creating_file_menu() const;
	void set_main_window_properties(int16_t, int16_t);
	void creating_ui_elements();
	wxUString get_folder_path();

private:
	enum class MY_ID
	{
		ID_PATH_TEXT_BOX,
		ID_PATH_TEXT_BOX_LABEL,
		ID_OUTPUT_TEXT_BOX_LABEL,
		ID_OUTPUT_TEXT_BOX,
		ID_TOOLBAR,
		ID_OPEN_BUTTON,
		ID_LIST
	};

	wxButton* list_button_{ nullptr };
	wxButton* open_button_ { nullptr };
	wxStaticText* path_input_field_label_{ nullptr };
	wxStaticText* result_output_field_label_{ nullptr };
	wxTextCtrl* path_text_box_{ nullptr };
	wxTextCtrl* result_text_box_{ nullptr };

	wxUString path_;

	DECLARE_EVENT_TABLE();
};
