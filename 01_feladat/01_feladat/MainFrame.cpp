#include "MainFrame.h"

#include <wx/button.h>
#include <wx/wfstream.h>

#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include <string>
#include <sstream>
#include <shlwapi.h>

#include "FileNameHandler.h"

/*********************************************
	Constructor/destructor
*********************************************/
MainFrame::MainFrame(const wxString& window_title) :
	wxFrame(nullptr, -1, window_title)
{
	this->set_main_window_properties(630, 500);

	wxFrameBase::SetMenuBar(this->creating_file_menu());

	wxFrameBase::CreateStatusBar(2);
	wxFrameBase::SetStatusText(wxT("Welcome!"));

	this->creating_ui_elements();
}

/**********************************************
	Member functions
**********************************************/

// Events
void MainFrame::on_about(wxCommandEvent& event)
{
	wxString msg;
	msg.Printf(wxT("This program is a personal project for a job finding purposes!"));

	wxMessageBox(msg, wxT("About minimal"), wxOK | wxICON_INFORMATION, this);
}

void MainFrame::on_quit(wxCommandEvent& event) { Close(); }

void MainFrame::on_list_button(wxCommandEvent& event)
{
	this->result_text_box_->Clear();

	// Getting the path from the input text
	wxString str_path{ this->path_text_box_->GetLineText(0) };
	const wchar_t* path{ str_path.c_str() };
	size_t path_size{ std::wstring(path).size() };

	WIN32_FIND_DATA ffd{};
	TCHAR sz_dir[MAX_PATH]{};
	HANDLE h_find{ INVALID_HANDLE_VALUE };
	DWORD dw_error{};

	if (str_path.IsEmpty())
	{
		SetStatusText(wxT("Error: please type a path in the path field!"));
		return;
	}

	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.
	StringCchLength(path, MAX_PATH, &path_size);

	if (path_size > (MAX_PATH - 3))
	{
		SetStatusText(wxT("\nDirectory path is too long.\n"));
		return;
	}

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.
	StringCchCopy(sz_dir, MAX_PATH, path);
	StringCchCat(sz_dir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.
	h_find = FindFirstFile(sz_dir, &ffd);

	if (INVALID_HANDLE_VALUE == h_find)
	{
		SetStatusText(wxT("Error: not a valid path"));
		return;
	}

	// get all the files and folders from the ffd and appending to the output text box
	SetStatusText(wxT("Listing in progress... please wait!"));
	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			// getting the file name, and formatting the output text
			wxString file_name{ ffd.cFileName };
			this->result_text_box_->AppendText(
				wxString::Format(
					wxT("%-4s | %d\n"),
					file_name,
					FileNameHandler::get_number_of_consonant(file_name)
				)
			);
		}
		else
		{
			// same as in the true path
			wxString file_name{ ffd.cFileName };
			this->result_text_box_->AppendText(
				wxString::Format(
					wxT("%-4s | %6d byte | %s | %d\n"),
					file_name,
					static_cast<int>(ffd.nFileSizeLow),
					FileNameHandler::get_file_extension(file_name),
					FileNameHandler::get_number_of_consonant(file_name)
				)
			);
		}
	} while (FindNextFile(h_find, &ffd) != 0);

	dw_error = GetLastError();
	if (dw_error != ERROR_NO_MORE_FILES)
	{
		SetStatusText(wxT("No more file"));
	}

	FindClose(h_find);
	SetStatusText(wxT("Listing finished!"));

}

void MainFrame::on_open_button(wxCommandEvent& event)
{
	SetStatusText(wxT("Open button cliecked"));

	this->path_ = this->get_folder_path();

	wxUString path_text = this->path_;

	this->path_text_box_->Clear();
	this->path_text_box_->WriteText(path_text);
}

// Helper functions
wxMenuBar* MainFrame::creating_file_menu() const
{
	wxMenuBar* menu_bar{ new wxMenuBar{} };

	wxMenu* file_menu{ new wxMenu{} };
	wxMenu* help_menu{ new wxMenu{} };

	file_menu->Append(
		wxID_EXIT,
		wxT("&Exit\tAlt-X"),
		wxT("Quit this program")
	);

	help_menu->Append(
		wxID_ABOUT,
		wxT("&About\tF1"),
		wxT("About the program")
	);

	menu_bar->Append(file_menu, wxT("&File"));
	menu_bar->Append(help_menu, wxT("&Help"));

	return menu_bar;
}

void MainFrame::set_main_window_properties(int16_t x, int16_t y)
{
	this->SetSize(x, y);
	this->SetMinSize(wxSize(x, y));
	this->SetMaxSize(wxSize(x, y));
}

void MainFrame::creating_ui_elements()
{
	this->path_input_field_label_ = new wxStaticText{
		this,
		static_cast<int>(MY_ID::ID_PATH_TEXT_BOX_LABEL),
		wxT("Path:"),
		wxPoint(20,20),
		wxDefaultSize
	};

	this->path_text_box_ = new wxTextCtrl{
		this,
		static_cast<int>(MY_ID::ID_PATH_TEXT_BOX),
		wxEmptyString,
		wxPoint(20,40),
		wxSize(375, 25),
		0L,
		wxDefaultValidator,
		wxTextCtrlNameStr
	};

	path_text_box_->WriteText(wxT("C:\\"));

	this->list_button_ = new wxButton{
		this,
		static_cast<int>(MY_ID::ID_LIST),
		wxT("List"),
		wxPoint(500, 40)
	};

	this->result_output_field_label_ = new wxStaticText{
		this,
		static_cast<int>(MY_ID::ID_OUTPUT_TEXT_BOX_LABEL),
		wxT("Output:"),
		wxPoint(20, 75)
	};

	this->result_text_box_ = new wxTextCtrl{
		this,
		static_cast<int>(MY_ID::ID_OUTPUT_TEXT_BOX),
		wxEmptyString,
		wxPoint(20, 95),
		wxSize(570, 300),
		wxTE_MULTILINE
	};

	this->result_text_box_->SetEditable(false);

	this->open_button_ = new wxButton{
		this,
		static_cast<int>(MY_ID::ID_OPEN_BUTTON),
		wxT("Open"),
		wxPoint(400,40),
		wxDefaultSize,
		0L,
		wxDefaultValidator,
		wxT("Open folder")
	};
}

wxUString MainFrame::get_folder_path()
{
	wxDirDialog* folder_select_dialog = new wxDirDialog{
		nullptr,
		wxT("Please select a foler!"),
		wxT("C:\\")
	};

	if (folder_select_dialog->ShowModal() == wxID_CANCEL)
	{
		folder_select_dialog->Destroy();
		return "";
	}

	std::wstring temp_str_path{ folder_select_dialog->GetPath() };
	LPCWSTR folder_path{ temp_str_path.c_str() };

	if (!PathFileExistsW(folder_path))
	{
		SetStatusText(wxT("Invalid path!"));
		folder_select_dialog->Destroy();
		return "";
	}

	folder_select_dialog->Destroy();

	return wxUString(folder_path);
}

/*********************************************
	Event table
*********************************************/
BEGIN_EVENT_TABLE(MainFrame, wxFrame)
EVT_MENU(wxID_ABOUT, MainFrame::on_about)
EVT_MENU(wxID_EXIT, MainFrame::on_quit)
EVT_BUTTON(static_cast<int>(MY_ID::ID_LIST), MainFrame::on_list_button)
EVT_BUTTON(static_cast<int>(MY_ID::ID_OPEN_BUTTON), MainFrame::on_open_button)
END_EVENT_TABLE()