#include <wx/wx.h>

#include "MainFrame.h"

class MyApp : public wxApp
{
public:
	bool OnInit() override
	{
		MainFrame* main_frame { new MainFrame{wxT("First Project")} };

		main_frame->Show(true);

		return true;
	}
};

DECLARE_APP(MyApp);

IMPLEMENT_APP(MyApp);