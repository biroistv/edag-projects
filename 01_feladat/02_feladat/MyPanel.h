#pragma once

#include <wx/wx.h>

class MyPanel : public wxPanel
{
public:
	MyPanel(wxWindow*, int, const wxString&);
	~MyPanel() = default;

	void on_panel_click(wxMouseEvent&);

	void clear_drawing_area();
	void generate_triangle();

	void set_text_box_pointers(wxTextCtrl*, wxTextCtrl*, wxTextCtrl*);
	void set_text_box_value(wxTextCtrl*, int, int);

private:
	wxTextCtrl* a{};
	wxTextCtrl* b{};
	wxTextCtrl* c{};

	std::vector<wxPoint> points_vector{};

	DECLARE_EVENT_TABLE();
};

