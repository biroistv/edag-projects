#include "MyFrame.h"

class SecondProject : public wxApp
{
public:
	bool OnInit() override
	{
		MyFrame* window { new MyFrame{wxT("Second project")}};

		window->Show(true);

		return true;
	}
};

DECLARE_APP(SecondProject);

IMPLEMENT_APP(SecondProject);