#include "MyPanel.h"


/********************************************
	Constructor/Destructor
********************************************/
MyPanel::MyPanel(wxWindow* parent, const int id, const wxString& name = wxT("custom_panel")) :
	wxPanel(parent, id, wxDefaultPosition, wxDefaultSize, 0L, name)
{
	this->wxWindowBase::SetBackgroundColour(wxColor(255, 255, 255));
}

/********************************************
	Event functions
********************************************/

void MyPanel::on_panel_click(wxMouseEvent& event)
{
	if (points_vector.size() < 3)
	{
		const wxPoint clicked_position{
		wxGetMousePosition().x - this->GetScreenPosition().x,
		wxGetMousePosition().y - this->GetScreenPosition().y
		};

		points_vector.push_back(clicked_position);

		switch (points_vector.size())
		{
		case 1:
			{
				this->set_text_box_value(a, clicked_position.x, clicked_position.y);
				break;
			}
		case 2:
			{
				this->set_text_box_value(b, clicked_position.x, clicked_position.y);
				break;
			}
		case 3:
			{
				this->set_text_box_value(c, clicked_position.x, clicked_position.y);
				break;
			}
		default: break;
		}
	}
	else
	{
		wxMessageBox(
			wxT("Already selected three point! Click the \"Generate\" button\nto generate the triangle.")
		);
	}
}

/********************************************
	Member functions
*********************************************/

void MyPanel::generate_triangle()
{
	if (this->points_vector.size() != 3)
	{
		wxMessageBox(
			wxT("Cant generate the triangle with less then 3 top!")
		);
	}
	else
	{
		wxClientDC dc(this);
		const wxPen pen(*wxBLACK, 1);

		wxPoint mouse_pos{
			wxGetMousePosition().x - this->GetScreenPosition().x,
			wxGetMousePosition().y - this->GetScreenPosition().y
		};

		const wxPoint a_top{ this->points_vector.at(0) };
		const wxPoint b_top{ this->points_vector.at(1) };
		const wxPoint c_top{ this->points_vector.at(2) };

		dc.DrawLine(a_top, b_top);
		dc.DrawLine(b_top, c_top);
		dc.DrawLine(c_top, a_top);

		this->points_vector.clear();
	}
}

void MyPanel::set_text_box_pointers(wxTextCtrl* a, wxTextCtrl* b, wxTextCtrl* c)
{
	this->a = a;
	this->b = b;
	this->c = c;
}

void MyPanel::set_text_box_value(wxTextCtrl* txt_box, int x, int y)
{
	txt_box->Clear();
	txt_box->WriteText(wxString::Format(
					wxT("(%d,%d)"),
					x,
					y
				));
}

void MyPanel::clear_drawing_area()
{
	this->Refresh(true);
}

BEGIN_EVENT_TABLE(MyPanel, wxWindow)
EVT_LEFT_UP(MyPanel::on_panel_click)
END_EVENT_TABLE()