#include "MyFrame.h"

/********************************************
	Constructor/destructor
********************************************/
MyFrame::MyFrame(const wxString& title) :
	wxFrame(nullptr, -1, title)
{
	this->set_window_size(540, 680);
	this->set_file_menu();
	this->adding_gui_parts();

	wxFrameBase::CreateStatusBar(2, 0L, 0, wxT("status_bar"));
	wxFrameBase::SetStatusText(wxT("Welcome!"));
}

/********************************************
	Event functions
********************************************/
void MyFrame::on_exit(wxCommandEvent& event) { this->Close(); }

void MyFrame::on_about(wxCommandEvent& event)
{
	wxMessageBox(
		wxT("Second project, more details on the github repo!")
	);
}

void MyFrame::on_generate(wxCommandEvent& event)
{
	this->drawing_area_->generate_triangle();
}

void MyFrame::on_clear(wxCommandEvent& event)
{
	this->drawing_area_->clear_drawing_area();

	this->side_a_text_box_->Clear();
	this->side_a_text_box_->WriteText(wxT("(0.0)"));
	this->side_b_text_box_->Clear();
	this->side_b_text_box_->WriteText(wxT("(0.0)"));
	this->side_c_text_box_->Clear();
	this->side_c_text_box_->WriteText(wxT("(0.0)"));
}

/********************************************
	 Member functions
********************************************/
void MyFrame::set_file_menu()
{
	wxMenuBar* menu_bar{ new wxMenuBar{} };

	wxMenu* file_menu{ new wxMenu{} };
	wxMenu* help_menu{ new wxMenu{} };

	file_menu->Append(
		wxID_EXIT,
		wxT("&Quit\tAlt+X"),
		wxT("Close the application")
	);

	help_menu->Append(
		wxID_ABOUT,
		wxT("&About\tF1"),
		wxT("About minimal")
	);

	menu_bar->Append(file_menu, wxT("File"));
	menu_bar->Append(help_menu, wxT("Help"));

	this->SetMenuBar(menu_bar);
}

void MyFrame::adding_gui_parts()
{
	/*
		Buttons
	******************************/
	this->generate_button_ = new wxButton{
		this,
		static_cast<int>(MyID::GENERATE_BUTTON),
		wxT("Generate"),
		wxPoint(300, 30),
		wxDefaultSize,
		0,
		wxDefaultValidator,
		wxT("generate_button")
	};

	this->clear_button_ = new wxButton{
		this,
		static_cast<int>(MyID::CLEAR_BUTTON),
		wxT("Clear"),
		wxPoint(400, 30),
		wxDefaultSize,
		0,
		wxDefaultValidator,
		wxT("clear_button")
	};

	/*
		side A fields
	******************************/
	this->side_a_label_ = new wxStaticText{
		this,
		static_cast<int>(MyID::SIDE_A_TEXT_BOX),
		wxT("a:"),
		wxPoint(30,33),
		wxDefaultSize
	};

	this->side_a_text_box_ = new wxTextCtrl{
		this,
		static_cast<int>(MyID::SIDE_A_TEXT_BOX),
		wxT("(0,0)"),
		wxPoint(45, 30),
		wxSize(60, 25),
		0L
	};

	this->side_a_text_box_->SetEditable(false);

	/*
		side B fields
	******************************/

	this->side_a_label_ = new wxStaticText{
		this,
		static_cast<int>(MyID::SIDE_A_TEXT_BOX),
		wxT("b:"),
		wxPoint(115,33),
		wxDefaultSize
	};

	this->side_b_text_box_ = new wxTextCtrl{
		this,
		static_cast<int>(MyID::SIDE_B_TEXT_BOX),
		wxT("(0,0)"),
		wxPoint(130, 30),
		wxSize(60, 25),
		0L
	};

	this->side_b_text_box_->SetEditable(false);

	/*
		side C fields
	******************************/

	this->side_a_label_ = new wxStaticText{
		this,
		static_cast<int>(MyID::SIDE_A_TEXT_BOX),
		wxT("c:"),
		wxPoint(200,33),
		wxDefaultSize
	};

	this->side_c_text_box_ = new wxTextCtrl{
		this,
		static_cast<int>(MyID::SIDE_B_TEXT_BOX),
		wxT("(0,0)"),
		wxPoint(215, 30),
		wxSize(60, 25),
		0L
	};

	this->side_c_text_box_->SetEditable(false);

	/*
		drawing panel
	*****************************/
	this->drawing_area_ = new MyPanel{
		this,
		static_cast<int>(MyID::DRAWING_AREA),
		"drawing_area"
	};

	drawing_area_->SetPosition(wxPoint(15, 100));
	drawing_area_->SetSize(wxSize(490, 485));
	drawing_area_->set_text_box_pointers(
		this->side_a_text_box_, 
		this->side_b_text_box_,
		this->side_c_text_box_
	);

	/*
		Static box
	*****************************/
	wxStaticBox* top_static_box{ new wxStaticBox{
		this,
		-1,
		wxT("Current triangle top positions:"),
		wxPoint(10,10),
		wxSize(275, 60)
	} };

	wxStaticBox* drawing_static_box{ new wxStaticBox{
		this,
		-1,
		wxT("Drawing area:"),
		wxPoint(10,80),
		wxSize(500, 510)
	} };
}

void MyFrame::set_window_size(uint16_t x, uint16_t y)
{
	this->SetSize(wxSize(x, y));
	this->SetMinSize(wxSize(x, y));
	this->SetMaxSize(wxSize(x, y));
}

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
	EVT_MENU(wxID_EXIT, MyFrame::on_exit)
	EVT_MENU(wxID_ABOUT, MyFrame::on_about)
	EVT_BUTTON(static_cast<int>(MyID::GENERATE_BUTTON), MyFrame::on_generate)
	EVT_BUTTON(static_cast<int>(MyID::CLEAR_BUTTON), MyFrame::on_clear)
END_EVENT_TABLE()