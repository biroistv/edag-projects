#pragma once

#include <wx/wx.h>
#include "MyPanel.h"

class MyFrame : public wxFrame
{
public:
	MyFrame(const wxString&);
	~MyFrame() = default;

public:
	void on_exit(wxCommandEvent&);
	void on_about(wxCommandEvent&);
	void on_generate(wxCommandEvent&);
	void on_clear(wxCommandEvent&);

private:
	DECLARE_EVENT_TABLE();

	enum class MyID
	{
		SIDE_A_TEXT_BOX,
		SIDE_B_TEXT_BOX,
		SIDE_C_TEXT_BOX,
		SIDE_A_LABEL,
		SIDE_B_LABEL,
		SIDE_C_LABEL,
		GENERATE_BUTTON,
		CLEAR_BUTTON,
		DRAWING_AREA
	};

	// Input fields
	wxTextCtrl* side_a_text_box_ { nullptr };
	wxTextCtrl* side_b_text_box_ { nullptr };
	wxTextCtrl* side_c_text_box_ { nullptr };

	wxStaticText* side_a_label_ { nullptr };
	wxStaticText* side_b_label_ { nullptr };
	wxStaticText* side_c_label_ { nullptr };

	MyPanel* drawing_area_ { nullptr };

	wxButton* generate_button_{ nullptr };
	wxButton* clear_button_{ nullptr };

private:
	void set_file_menu();
	void adding_gui_parts();
	void set_window_size(uint16_t, uint16_t);
};

