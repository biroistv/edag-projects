# Edag projects

In this repository there a three project, wich are part of a job interview. All the project are writen in the C++ programming language. To visualize the program in a graphical user interface i use a third-party library, wxWidgets. In the next section there are some sort description for each project and what implemented and what not.
Minimum system requirement of all the program:
	32 bit Windows 7 operating system
	
---

## First poject - File handling

- [x] The program list the contents of the folder. Displaying the file name, type, size and the number of the consonant in the file name.  
- [ ] Fill a tree data structure with all the sub folders under the root folder
- [ ] Serching in the tree

[Download](https://1drv.ms/u/s!AuDrjjr0txMG23s-2VmQJQ6IdGju)
- unpack the zip file and run the 01_feladat.exe

### Know issues
- Not working properly with special characters

---

## Secound project - Drawing

- [x] Displaying a triangle on a panel
- [ ] The user can rotate triangle on the x axis with a custom value 
- [ ] The user can rotate the triangle with a slider between 0-180

[Download](https://1drv.ms/u/s!AuDrjjr0txMG23yZECJbciDkMcgC)
- unpack the zip file and run the 02_feladat.exe

### How to use
Click set three point on the drawing are by clicking on it three times. Then click the Generate button to generate the triangle. To clear the drawing are click on the Clear button.

### Know issues

---

## Third project - usage of the OOP in C++

- [x] Define and declare a object model of a car. For example Vehicle superclass with some member function and member variable and a Car class wich is inherited from the vehicle class. Need defining some friend and chield object.

The answer is found o nthe next link: [UML object diagram](https://1drv.ms/b/s!AuDrjjr0txMG23_vmG9PsXkQDz8E)

---

## Forth task - prove that i can understand what i do

- [x] Demonstrate a program what i wrote.
- [x] Best project, what was the theme?

The answer is found o nthe next link: [UML object diagram](https://1drv.ms/b/s!AuDrjjr0txMG234gGCF88ZtvaEaY)

